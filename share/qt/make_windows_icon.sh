#!/bin/bash
# create multiresolution windows icon
ICON_DST=../../src/qt/res/icons/Gic.ico

convert ../../src/qt/res/icons/Gic-16.png ../../src/qt/res/icons/Gic-32.png ../../src/qt/res/icons/Gic-48.png ${ICON_DST}
